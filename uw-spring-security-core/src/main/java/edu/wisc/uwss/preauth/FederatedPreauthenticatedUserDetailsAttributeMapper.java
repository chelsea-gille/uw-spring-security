package edu.wisc.uwss.preauth;

import edu.wisc.uwss.UWUserDetails;
import edu.wisc.uwss.UWUserDetailsImpl;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Enumeration;

/**
 * {@link PreauthenticatedUserDetailsAttributeMapper} implementation tailored to UW System user login.
 * Binds well known {@link HttpServletRequest#getHeader(String)} names that are
 * exported via common Shibboleth SP configurations used in the UW System.
 *
 * @author Benjamin Sousa
 */
public class FederatedPreauthenticatedUserDetailsAttributeMapper implements PreauthenticatedUserDetailsAttributeMapper {

    private String eppnHeader = "eppn";
    private String pviHeader = "eduWisconsinSPVI";
    private String usernameHeader = "eppn";
    private String fullNameHeader = "cn";
    private String uddsHeader = "eduWisconsinUDDS";
    private String emailAddressHeader = "mail";
    private String firstNameHeader = "givenName";
    private String lastNameHeader = "sn";
    private String identityProviderHeader = "Shib-Identity-Provider";
    private String customLogoutPrefix = "/Shibboleth.sso/Logout?return=";
    private String customLogoutSuffix = "/logout/";

    private static final Logger logger = LoggerFactory.getLogger(FederatedPreauthenticatedUserDetailsAttributeMapper.class);
    /**
     * {@inheritDoc}
     *
     * Will short-circuit and return null if {@link HttpServletRequest#getHeader(String)} for {@link #getEppnHeader()}
     * returns null.
     *
     * Instances returned by this method will always return an empty {@link String} for {@link UWUserDetails#getPassword()}.
     */
    @Override
    public UWUserDetails mapUser(HttpServletRequest request) {
        logger.debug("enter mapUser, available headers: {}", request.getHeaderNames());
        String eppn = request.getHeader(eppnHeader);
        logger.debug("enter mapUser, eppnHeader {} was {}", eppnHeader, eppn);
        if(StringUtils.isBlank(eppn)) {
            logger.debug("eppnHeader {} was blank, returning null", eppnHeader);
            return null;
        }
        String pvi = request.getHeader(pviHeader);
        String cn = request.getHeader(fullNameHeader);
        String userName = request.getHeader(usernameHeader);

        Collection<String> uddsMembership = new ArrayList<>();
        Enumeration<String> uddsHeaders = request.getHeaders(uddsHeader);
        if(uddsHeaders != null) {
            uddsMembership = Collections.list(uddsHeaders);
        }
        String email = request.getHeader(emailAddressHeader);
        UWUserDetailsImpl result = new UWUserDetailsImpl(pvi, userName, "", cn, email, uddsMembership);
        result.setSource("edu.wisc.uwss.preauth.federation");
        result.setEppn(eppn);
        result.setFirstName(request.getHeader(firstNameHeader));
        result.setLastName(request.getHeader(lastNameHeader));
        String identityProvider = request.getHeader(identityProviderHeader);
        result.setCustomLogoutUrl(toCustomLogoutUrl(identityProvider));

        logger.debug("mapUser constructed {} from headers in request", result);
        return result;
    }

    /**
     *
     * @param identityProviderHeaderValue the value of the identityProviderHeader
     * @return the corresponding log out url, or null
     */
    protected String toCustomLogoutUrl(String identityProviderHeaderValue) {
        String result = null;
        if(StringUtils.isNotBlank(identityProviderHeaderValue)) {
            try {
                URL url = new URL(identityProviderHeaderValue);
                String path = url.getPath();
                if(StringUtils.isBlank(path)) {
                    result = customLogoutPrefix + identityProviderHeaderValue + customLogoutSuffix;
                } else {
                    result = customLogoutPrefix + identityProviderHeaderValue.replace(url.getPath(), customLogoutSuffix);
                }

            } catch (MalformedURLException e) {
                logger.debug("caught MalformedURLException for identityProviderHeader={}", identityProviderHeaderValue, e);
            }
        }
        logger.debug("value of {} was {} and transformed to customLogoutUrl of {}", identityProviderHeader, identityProviderHeaderValue, result);
        return result;
    }
    /**
     * @return the eppnHeader
     */
    public String getEppnHeader() {
        return eppnHeader;
    }
    /**
     * @param eppnHeader the eppnHeader to set
     */
    @Value("${edu.wisc.uwss.preauth.federation.eppnHeader:eppn}")
    public void setEppnHeader(String eppnHeader) {
        this.eppnHeader = eppnHeader;
    }
    /**
     * @return the pviHeader
     */
    public String getPviHeader() {
        return pviHeader;
    }
    /**
     * @param pviHeader the pviHeader to set
     */
    @Value("${edu.wisc.uwss.preauth.federation.pviHeader:eduWisconsinSPVI}")
    public void setPviHeader(String pviHeader) {
        this.pviHeader = pviHeader;
    }
    /**
     * @return the usernameHeader
     */
    public String getUsernameHeader() {
    return usernameHeader;
    }
    /**
     * @param usernameHeader the usernameHeader to set
     */
     @Value("${edu.wisc.uwss.preauth.federation.usernameHeader:eppn}")
     public void setUsernameHeader(String usernameHeader) {
     this.usernameHeader = usernameHeader;
     }
     /**
      * @return the fullNameHeader
     */
    public String getFullNameHeader() {
        return fullNameHeader;
    }
    /**
     * @param fullNameHeader the fullNameHeader to set
     */
    @Value("${edu.wisc.uwss.preauth.federation.fullnameHeader:cn}")
    public void setFullNameHeader(String fullNameHeader) {
        this.fullNameHeader = fullNameHeader;
    }
    /**
     * @return the uddsHeader
     */
    public String getUddsHeader() {
        return uddsHeader;
    }
    /**
     * @param uddsHeader the uddsHeader to set
     */
    @Value("${edu.wisc.uwss.preauth.federation.uddsHeader:eduWisconsinUDDS}")
    public void setUddsHeader(String uddsHeader) {
        this.uddsHeader = uddsHeader;
    }
    /**
     * @return the emailAddressHeader
     */
    public String getEmailAddressHeader() {
        return emailAddressHeader;
    }
    /**
     * @param emailAddressHeader the emailAddressHeader to set
     */
    @Value("${edu.wisc.uwss.preauth.federation.emailAddressHeader:mail}")
    public void setEmailAddressHeader(String emailAddressHeader) {
        this.emailAddressHeader = emailAddressHeader;
    }
    /**
     * @return the identityProviderHeader
     */
    public String getIdentityProviderHeader() {
        return identityProviderHeader;
    }
    /**
     * @param identityProviderHeader the identityProviderHeader to set
     */
    @Value("${edu.wisc.uwss.preauth.federation.identityProviderHeader:Shib-Identity-Provider}")
    public void setIdentityProviderHeader(String identityProviderHeader) {
        this.identityProviderHeader = identityProviderHeader;
    }
    /**
     * @return the customLogoutPrefix
     */
    public String getCustomLogoutPrefix() {
        return customLogoutPrefix;
    }
    /**
     * @param customLogoutPrefix the customLogoutPrefix to set
     */
    @Value("${edu.wisc.uwss.preauth.federation.customLogoutPrefix:/Shibboleth.sso/Logout?return=}")
    public void setCustomLogoutPrefix(String customLogoutPrefix) {
        this.customLogoutPrefix = customLogoutPrefix;
    }
    /**
     * @return the customLogoutSuffix
     */
    public String getCustomLogoutSuffix() {
        return customLogoutSuffix;
    }
    /**
     * @param customLogoutSuffix the customLogoutSuffix to set
     */
    @Value("${edu.wisc.uwss.preauth.federation.customLogoutSuffix:/logout/}")
    public void setCustomLogoutSuffix(String customLogoutSuffix) {
        this.customLogoutSuffix = customLogoutSuffix;
    }
     /**
      * @return the firstNameHeader
     */
    public String getFirstNameHeader() {
        return firstNameHeader;
    }
    /**
     * @param firstNameHeader the firstNameHeader to set
     */
    @Value("${edu.wisc.uwss.preauth.federation:givenName}")
    public void setFirstNameHeader(String firstNameHeader) {
        this.firstNameHeader = firstNameHeader;
    }
    /**
     * @return the lastNameHeader
     */
    public String getLastNameHeader() {
        return lastNameHeader;
    }
    /**
     * @param lastNameHeader the lastNameHeader to set
     */
    @Value("${edu.wisc.uwss.preauth.federation:sn}")
    public void setLastNameHeader(String lastNameHeader) {
        this.lastNameHeader = lastNameHeader;
    }
}
