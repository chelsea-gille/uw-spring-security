package edu.wisc.uwss.local;


import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;

import org.springframework.core.io.Resource;

import java.io.IOException;
import java.util.List;

import edu.wisc.uwss.UWUserDetails;
import edu.wisc.uwss.UWUserDetailsImpl;
import edu.wisc.uwss.UWSpringSecurityException;

/**
 * Interface for loading {@link UWUserDetails} instances from configuration.
 *
 * This interface is used during application initialization - not during
 * authentication attempts. Since it is executed during Spring ApplicationContext initialization,
 * implementations should avoid injecting other service or dao interfaces, as it may be
 * affected by a race condition.
 *
 * If you have custom {@link UWUserDetails} that depend on services/daos to complete the model,
 * you may want to consider implementing a {@link LocalUsersAuthenticationAttemptCallback}; that interface
 * participates in the authentication attempt itself, not during application initialization.
 *
 * @see {@link LocalUserDetailsLoader.Default}
 * @author Nicholas Blair
 */
public interface LocalUserDetailsLoader {

  /**
   * @param resource a {@link Resource} with an accessible {@link java.io.InputStream}
   * @return a never null, but potentially empty, {@link List} of {@link UWUserDetails} instances
   * @throws UWSpringSecurityException if the {@link Resource} couldn't be read
   */
  List<? extends UWUserDetails> loadUsers(Resource resource);

  /**
   * Default loader uses Jackson and can parse both JSON and YAML formats.
   */
  public static class Default implements LocalUserDetailsLoader {

    private ObjectMapper objectMapper = new ObjectMapper(new YAMLFactory());

    /**
     * Visible for subclasses to override.
     *
     * @return the Jackson {@link ObjectMapper} used by {@link #loadUsers(Resource)}
     */
    protected ObjectMapper getObjectMapper() {
      return objectMapper;
    }
    @Override
    public List<? extends UWUserDetails> loadUsers(Resource resource) {
      try {
        List<? extends UWUserDetails> users = getObjectMapper().readValue(resource.getInputStream(), new TypeReference<List<UWUserDetailsImpl>>(){});
        return users;
      } catch (IOException e) {
        throw new UWSpringSecurityException("failed to read " + resource, e);
      }
    }
  }
}
