package edu.wisc.uwss.web.shibboleth;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Part of {@link SimulatedResponse}.
 *
 * @author Nicholas Blair
 */
public class Attribute {

  private final String name;
  private final List<String> values;

  public Attribute(String name, String value) {
    this(name, Arrays.asList(value));
  }
  public Attribute(String name, List<String> values) {
    this.name = name;
    this.values = Collections.unmodifiableList(values);
  }

  public String getName() {
    return name;
  }

  public List<String> getValues() {
    return values;
  }
}
