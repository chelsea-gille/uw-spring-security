/**
 * 
 */
package edu.wisc.uwss.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.Map;

import edu.wisc.uwss.UWUserDetails;
import edu.wisc.uwss.web.uwframe.UwframeSession;

/**
 * {@link Controller} useful for returning the current authenticated principal.
 *
 * @see ProfileRequiresAuthenticationHttpSecurityAmender
 * @author Nicholas Blair
 */
@Controller
public class ProfileController {
  @Autowired
  private Environment environment;

  /**
   * This method intentionally avoids using {@link edu.wisc.uwss.UWUserDetails} for the argument and
   * return type. Configurations exist that may result authentication not being required for this URL.
   *
   * @return the current authenticated principal
   */
  @RequestMapping(value="/profile", method=RequestMethod.GET)
  public @ResponseBody Object profile(@AuthenticationPrincipal Object principal) {
    return principal;
  }

  /**
   * This method transforms the {@link UWUserDetails} (if present) to a {@link UwframeSession},
   * Intended for use in uw-frame applications.
   *
   * To register this url in your uw-frame app, create the file 'src/main/webapp/js/override.js' with the following
   * contents:
   <pre>
   define(['angular'], function(angular) {
     var config = angular.module('override', []);
     config
       .constant('OVERRIDE', {
         'SERVICE_LOC': {
           'sessionInfo' : 'profile/uw-frame',
         }
     });
     return config;
   });
   </pre>
   *
   * @param principal
   * @return the current authenticated principal as a {@link UwframeSession}
   */
  @RequestMapping(value="/profile/uw-frame", method=RequestMethod.GET)
  public @ResponseBody Object uwFrameCompatibleProfile(@AuthenticationPrincipal Object principal) {
    Map<String, UwframeSession> response = new HashMap<>();
    if(principal instanceof UWUserDetails) {
      response.put("person", new UwframeSession(environment, (UWUserDetails) principal));
    } else {
      response.put("person", new UwframeSession(environment));
    }
    return response;
  }

}
