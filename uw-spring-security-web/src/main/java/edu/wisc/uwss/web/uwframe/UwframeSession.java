package edu.wisc.uwss.web.uwframe;

import org.springframework.core.env.Environment;

import edu.wisc.uwss.UWUserDetails;

/**
 * Bean matching data model expected by <a href="https://github.com/UW-Madison-DoIT/uw-frame">uw-frame</a>.
 *
 * The session contains a mix of user attributes and product revision information:
 *
 <pre>
 {
   "person": {
     "userName": "admin",
     "sessionKey": "someRandomKey",
     "serverName": "localhost",
     "firstName" : "Bucky",
     "lastName" : "Badger",
     "displayName": "Bucky Badger",
     "email" : "bucky@wisc.edu",
     "version": "2.2.0"
   }
 }
 </pre>
 *
 * @author Nicholas Blair
 */
public class UwframeSession {

  public static final String GIT_BRANCH = "git.branch";
  public static final String GIT_COMMIT_ID_ABBREV = "git.commit.id.abbrev";
  public static final String GIT_TAGS = "git.tags";
  public static final String PROJECT_VERSION = "projectVersion";

  private final String username;
  private final String displayName;
  private final String firstName;
  private final String lastName;
  private final UwframeBuild build;

  /**
   * Construct a "guest" session with no revision information available.
   */
  public UwframeSession() {
    this(null);
  }
  /**
   * Construct a "guest" session
   *
   * @param environment source for revision information
   */
  public UwframeSession(Environment environment) {
    this(environment, "anonymousUser", "Guest", "Guest", "");
  }
  /**
   * Preferred constructor.
   *
   * @param environment
   * @param userDetails
   */
  public UwframeSession(Environment environment, UWUserDetails userDetails) {
    this(environment, userDetails.getUsername(), userDetails.getDisplayName()!=null ? userDetails.getDisplayName() : userDetails.getFullName(),
            userDetails.getFirstName(), userDetails.getLastName());
  }
  /**
   *
   * @param environment
   * @param username
   * @param displayName
   * @deprecated prefer constructor UwframeSession(Environment, UWUserDetails) instead
   */
  @Deprecated
  public UwframeSession(Environment environment, String username, String displayName) {
    this(environment, username, displayName, null, null);
  }

  public UwframeSession(Environment environment, String username, String displayName, String firstName, String lastName) {
    this.username = username;
    this.displayName = displayName;
    this.firstName = firstName;
    this.lastName = lastName;
    if(environment != null && environment.containsProperty(GIT_COMMIT_ID_ABBREV)) {
      this.build =  new UwframeBuild(environment.getProperty(GIT_COMMIT_ID_ABBREV), environment.getProperty(GIT_BRANCH), environment.getProperty(PROJECT_VERSION), environment.getProperty(GIT_TAGS));
    } else {
      this.build = new UwframeBuild();
    }
  }

  /**
   * @return the username
   */
  public String getUsername() {
    return username;
  }
  /**
   * @return always an empty String
   */
  public String getSessionKey() {
    return "";
  }
  /**
   * @return always an empty String
   */
  public String getServerName() {
    return "";
  }
  /**
   * @return the displayName
   */
  public String getDisplayName() {
    return displayName;
  }
  /**
   * @return the first name
   */
  public String getFirstName() {
    return firstName;
  }
  /**
   * @return the last name
   */
  public String getLastName() {
    return lastName;
  }
  /**
   * @return the version
   */
  public String getVersion() {
    return build.toVersionString();
  }
}
