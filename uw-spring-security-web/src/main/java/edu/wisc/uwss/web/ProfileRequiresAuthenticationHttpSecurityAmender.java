package edu.wisc.uwss.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.stereotype.Component;

import edu.wisc.uwss.configuration.HttpSecurityAmender;

/**
 * {@link HttpSecurityAmender} to make sure requests to /profile are authenticated.
 *
 * @see ProfileController
 * @author Nicholas Blair
 */
public class ProfileRequiresAuthenticationHttpSecurityAmender implements HttpSecurityAmender {
  static final Logger logger = LoggerFactory.getLogger(ProfileRequiresAuthenticationHttpSecurityAmender.class);
  @Override
  public void amend(HttpSecurity httpSecurity) throws Exception {
    httpSecurity
            .authorizeRequests()
              .antMatchers("/profile").authenticated();
    logger.info("url '/profile' requires authentication");
  }
}
