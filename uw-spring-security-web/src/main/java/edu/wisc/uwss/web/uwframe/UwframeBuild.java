/**
 * 
 */
package edu.wisc.uwss.web.uwframe;

import org.apache.commons.lang.StringUtils;

/**
 * Bean that mimics the data model uw-frame expects for the application's build information.
 * 
 * @author Nicholas Blair
 */
public class UwframeBuild {

  protected String buildNumber;
  protected String scmBranch;
  protected String projectVersion;
  protected String tags;

  public UwframeBuild() {
  }
  /**
   * 
   * @param buildNumber the git commitId
   * @param scmBranch the git branch (can be null)
   * @param projectVersion the maven project version
   * @param tags the tags (can be null)
   */
  public UwframeBuild(String buildNumber, String scmBranch, String projectVersion, String tags) {
    this.buildNumber = buildNumber;
    this.scmBranch = scmBranch;
    this.projectVersion = projectVersion;
    this.tags = tags;
  }
  /**
   * @return the buildNumber
   */
  public String getBuildNumber() {
    return buildNumber;
  }
  /**
   * @param buildNumber the buildNumber to set
   */
  public UwframeBuild setBuildNumber(String buildNumber) {
    this.buildNumber = buildNumber;
    return this;
  }
  /**
   * @return the scmBranch
   */
  public String getScmBranch() {
    return scmBranch;
  }
  /**
   * @param scmBranch the scmBranch to set
   */
  public UwframeBuild setScmBranch(String scmBranch) {
    this.scmBranch = scmBranch;
    return this;
  }
  /**
   * @return the projectVersion
   */
  public String getProjectVersion() {
    return projectVersion;
  }
  /**
   * @param projectVersion the projectVersion to set
   */
  public UwframeBuild setProjectVersion(String projectVersion) {
    this.projectVersion = projectVersion;
    return this;
  }

  /**
   * @return the tags
   */
  public String getTags() {
    return tags;
  }
  /**
   * @param tags the tags to set
   */
  public UwframeBuild setTags(String tags) {
    this.tags = tags;
    return this;
  }

  /**
   * Returns an empty string if not all necessary fields are set.
   *
   * @return the fields as a single, human readable string
   */
  public String toVersionString() {
    if(StringUtils.isBlank(buildNumber)) {
      return "";
    }
    StringBuilder builder = new StringBuilder();
    builder.append("Revision ");
    builder.append(buildNumber);
    if(StringUtils.isNotBlank(projectVersion)) {
      builder.append(" (Version ");
      builder.append(projectVersion);
      if (StringUtils.isBlank(tags) && StringUtils.isNotBlank(scmBranch)) {
        builder.append(" from ");
        builder.append(scmBranch);
        builder.append(" branch");
      }
      builder.append(")");
    }
    return builder.toString();
  }
}
