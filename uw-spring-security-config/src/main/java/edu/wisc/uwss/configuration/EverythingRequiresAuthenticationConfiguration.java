/**
 * 
 */
package edu.wisc.uwss.configuration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;

/**
 * Convenience {@link Configuration} that imports {@link UWSpringSecurityConfiguration}
 * and sets up the {@link HttpSecurity} to require authentication for all URLs
 * within the servlet context.
 * 
 * @author Nicholas Blair
 */
@Configuration
@Import(UWSpringSecurityConfiguration.class)
public class EverythingRequiresAuthenticationConfiguration {

  static final Logger logger = LoggerFactory.getLogger(EverythingRequiresAuthenticationConfiguration.class);
  
  /**
   * Overrides the default {@link HttpSecurityAmender} with {@link EverythingRequiresAuthenticationHttpSecurityAmender}.
   * 
   * @return a {@link EverythingRequiresAuthenticationHttpSecurityAmender}.
   */
  @Bean
  public HttpSecurityAmender amender() {
    return new EverythingRequiresAuthenticationHttpSecurityAmender();
  }
  /**
   * This bean is required for {@link org.springframework.beans.factory.annotation.Value} annotations to be able to accept placeholders.
   *
   * @return a {@link PropertySourcesPlaceholderConfigurer}
   */
  @Bean
  public static PropertySourcesPlaceholderConfigurer propertyPlaceholderConfigurer() {
      return new PropertySourcesPlaceholderConfigurer();
  }
}
