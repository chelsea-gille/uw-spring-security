/**
 * 
 */
package edu.wisc.uwss.configuration.local;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.userdetails.UserDetailsService;

import edu.wisc.uwss.configuration.HttpSecurityAmender;
import edu.wisc.uwss.configuration.combined.CombinedAuthentication;

/**
 * {@link Configuration} for the "local-users" {@link Profile}.
 * 
 * @author Nicholas Blair
 */
@Configuration
@EnableWebSecurity
@Profile({"local-users", "edu.wisc.uwss.local-users"})
@Order(CombinedAuthentication.LOCAL_USERS_WEB_SECURITY_CONFIGURATION_ORDER)
public class LocalUsersWebSecurityConfiguration  {

  protected final Logger logger = LoggerFactory.getLogger(this.getClass());

  /**
   *
   * @return an {@link HttpSecurityAmender} that sets up formLogin and HTTP Basic
   */
  @Bean
  public HttpSecurityAmender localUsersHttpSecurityAmender() {
    return new HttpSecurityAmender() {
      @Override
      public void amend(HttpSecurity http) throws Exception {
        http
                .formLogin()
                .and()
                .httpBasic()
                .and()
                .logout()
                .invalidateHttpSession(true);

        // CSRF protection is disabled, as without it /logout requires POST
        http
                .csrf().disable();
      }
    };
  }
  /**
   * {@inheritDoc}
   * 
   * This method is required to properly inject the {@link UserDetailsService} provided by
   * LocalAuthenticationSecurityConfiguration into the AuthenticationManager.
   * 
   * @see AuthenticationManagerBuilder#userDetailsService(UserDetailsService)
   */
  @Autowired
  public void configureGlobal(AuthenticationManagerBuilder auth, UserDetailsService userDetailsService) throws Exception {
    logger.debug("enter WebSecurityConfigurerAdapter#configureGlobal for local-users, injecting {} into AuthenticationManagerBuilder", userDetailsService);
    auth.userDetailsService(userDetailsService);
  }

}
