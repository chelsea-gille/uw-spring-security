## Contributing to uw-spring-security

This project follows a collaboration workflow similar to what is described in the "Integration Manager" section of http://git-scm.com/book/en/Distributed-Git-Distributed-Workflows. 
The primary repository for this project is https://git.doit.wisc.edu/adi-ia/uw-spring-security/. Few developers will have 'push' access to this repository; the preferred permission will be 'fetch' only.

**No credentials for any remote systems are allowed in this project.** The only credentials found within this repository are those needed to communicate with the services on the local development environment.

Each developer should:

1. Fork the primary repository into their personal namespace.
2. Clone the fork to their workstation.
3. Create branches for each unit of work.
4. When done, push the branch up to their personal fork.
5. Submit a pull request from the branch in their personal fork to the primary repository.

This process allows us to make use of gitlab's code review tools and gives us the flexibility to pick and choose which features are ready for promotion at different times.

