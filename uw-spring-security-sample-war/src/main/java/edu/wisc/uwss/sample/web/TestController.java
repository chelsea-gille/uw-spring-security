package edu.wisc.uwss.sample.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * {@link Controller} providing test cases regarding required vs lazy authentication.
 *
 * @author Nicholas Blair
 */
@Controller
public class TestController {
  private static final Logger logger = LoggerFactory.getLogger(TestController.class);
  /**
   * This method is bound to a URL that requires authentication.
   *
   * @return the {@link #currentPrincipal()}
   */
  @RequestMapping(value="/required", method= RequestMethod.GET)
  public @ResponseBody Object authenticationRequired() {
    return currentPrincipal();
  }
  /**
   * This method is bound to a URL that does not require authentication.
   *
   * @return the {@link #currentPrincipal()}
   */
  @RequestMapping(value="/lazy", method=RequestMethod.GET)
  public @ResponseBody Object authenticationNotRequired() {
    return currentPrincipal();
  }
  /**
   *
   * @return the current value for {@link Authentication#getPrincipal()}, or null
   */
  protected Object currentPrincipal() {
    Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
    if(authentication == null) {
      return null;
    }
    Object principal = authentication.getPrincipal();
    logger.debug("Authentication#getPrincipal observed {}", principal);
    return principal;
  }
}
