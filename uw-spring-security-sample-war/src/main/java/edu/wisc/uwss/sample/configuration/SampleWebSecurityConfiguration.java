/**
 * 
 */
package edu.wisc.uwss.sample.configuration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;

import edu.wisc.uwss.configuration.HttpSecurityAmender;
import edu.wisc.uwss.web.ProfileRequiresAuthenticationHttpSecurityAmender;

/**
 * This {@link Configuration} class for the "local-users" {@link Profile} provides an example of how one would 
 * amend the {@link HttpSecurity} initialized in the various configuration classes in uw-spring-security-config.
 * 
 * @author Nicholas Blair
 */
@Configuration
public class SampleWebSecurityConfiguration {

  static final Logger logger = LoggerFactory.getLogger(SampleWebSecurityConfiguration.class);
  /**
   * This example puts a constraint on the "/required" and "/profile" URLs, requiring visitors be authenticated.
   *
   * @throws Exception see {@link HttpSecurity#authorizeRequests()}
   */
  @Bean
  public HttpSecurityAmender httpSecurityAmender() throws Exception {
    return new HttpSecurityAmender() {
      @Override
      public void amend(HttpSecurity http) throws Exception {
        logger.info("amendHttpSecurity: setting up access controls for known urls");
        http.authorizeRequests()
                .antMatchers("/favicon.ico").permitAll()
                .antMatchers("/index.html").permitAll()
                .antMatchers("/lazy").permitAll()
                .antMatchers("/required").authenticated();
      }
    };

  }

  /**
   *
   * @return {@link HttpSecurityAmender} that requires auth on /profile
   */
  @Bean
  public HttpSecurityAmender profileAuthentication() {
    return new ProfileRequiresAuthenticationHttpSecurityAmender();
  }
}
