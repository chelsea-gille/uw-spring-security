/**
 * 
 */
package edu.wisc.uwss.sample.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Simple Web MVC {@link Controller}.
 * 
 * @author Nicholas Blair
 */
@Controller
public class SampleController {

  /**
   * @return the home view
   */
  @RequestMapping(value="/", method=RequestMethod.GET)
  public String home() {
      return "redirect:/index.html";
  }
}
